var count = 0;
        function createCallback( i ){
          return function(){
            if ($("#btn"+i).children("img").attr("src") == "/static/images/love.png") {
                $("#div_animate" + i).html("<img id='animate" + i + "' src='/static/images/love.png' style='position: absolute; z-index: 2;' width='30' height='30'>");
                $("#animate" + i).animate({top: '380px', opacity:'0'});
                $("#btn"+i).children("img").prop("src", "/static/images/love_disabled.png");
                $("#btn"+i).prop("class", "btn btn-light");
                $("#btn"+i).css("backgroundColor", "#F65066");
                $("#favorite").html("<p>Favorite : " + ++count + "</p>");
            } else {
                $("#animate").stop();
                $("#btn"+i).children("img").prop("src", "/static/images/love.png");
                $("#btn"+i).prop("class", "btn btn-light");
                $("#btn"+i).css("backgroundColor", "");
                $("#favorite").html("<p>Favorite : " + --count + "</p>");
            }
          }
        }

        function search(name) {
           $.ajax({url: "/library/json/?find=" + name, success: function(result){
                    $("#total").html("<p>Jenis Buku : " + result.kind +  "<p>");
                    $("#total").append("<p>Total Buku : " + result.totalItems + "</p>");
                    $("#total").append("<p id='favorite'>Favorite : " + count + "</p>");
                    $("#add_favorite").html("<th>Add to Favorite</th>");
                    $("#preview").html("<th>Preview</th>");
                    $("#title").html("<th>Judul Buku</th>");
                    $("#description").html("<th>Keterangan</th>");
                    $("#author").html("<th>Penulis Buku</th>");
                    for (let i = 0; i < result.items.length; i++) {
                        var subtitle = "<td>" + "-" + "</td>";
                        if (result.items[i].volumeInfo.subtitle != null) subtitle = "<td>" + result.items[i].volumeInfo.subtitle + "</td>";
                        var button = "<td><button id='btn" + i + "' class='btn btn-light favorite' style='width: 10rem; '><img src='/static/images/love.png' style='padding: 5px; ' width='30' height='30'></button></td>";
                        var thumbnail = "<td>" + "<img src='/static/images/mark.jpg' width='200rem' />" + "</td>";
                        if (result.items[i].volumeInfo.imageLinks != null) thumbnail = "<td>" + "<img src='" + result.items[i].volumeInfo.imageLinks.thumbnail + "' />" + "</td>";
                        var title = "<td>" + result.items[i].volumeInfo.title + "</td>";
                        var author = "<td>" + result.items[i].volumeInfo.authors + "</td>";
                        $("#add_favorite").append(button);
                        $("#btn"+i).append("<div id='div_animate" + i + "'></div>");
                        $("#preview").append(thumbnail);
                        $("#title").append(title);
                        $("#description").append(subtitle);
                        $("#author").append(author);
                        $('#btn' + i).click( createCallback( i ) );
                    }
                }
            });
        }

        $(document).ready(function(){
            $(window).load(function() {
                search("hacker");

                $("#anime").click( function() {
                    search("anime");
                });
                $("#programmer").click( function() {
                   search("programmer"); 
                });
                $("#horror").click( function() {
                    search("horror");
                });
                $("#btn-search").click( function() {
                    if ($("#searchbar").val() != "") {
                        search($('#searchbar').val());
                    } else {
                        alert("Please insert what you are looking for !")
                    }
                });
                $("input").focus( function() {
                    $("#search").animate({width: "80rem"});
                });
                $("input").blur( function() {
                    $("#search").animate({width: "20rem"});
                });
            });

        });