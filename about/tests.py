from django.test import TestCase, Client
from django.urls import resolve
from .views import about

# Create your tests here.

class belajarfilm_about_testcase(TestCase):
    def test_belajarfilm_url_about_is_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_belajarfilm_using_about_function(self):
        found = resolve('/about/')
        self.assertEqual(found.func, about)