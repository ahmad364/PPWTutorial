from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .forms import subscribe_forms
from .models import subscribe_models

response = {}
# Create your views here.


def subscribe(request):
    if (request.method == "POST"):
        form = subscribe_forms(request.POST)
        response['form'] = form
    else:
        form = subscribe_forms()
        response['form'] = form
    if 'name' in request.session.keys():
        response['name'] = request.session['name']
    else:
        if 'name' in response.keys():
            response.__delitem__('name')
    return render(request, 'subscribe.html', response)


def subscribe_list(request):
    subscriber = subscribe_models.objects.all()
    list_subscriber = []
    for i in subscriber:
        temp = {'nama': i.nama, 'email': i.email}
        list_subscriber.append(temp)
    return JsonResponse(list_subscriber, safe=False)


@csrf_exempt
def delete_subscriber(request):
    if (request.method == "POST"):
        email = request.POST['email']
        print(email)
        subscribe_models.objects.get(email=email).delete()
    return HttpResponse("Ini cuma buat delete aja.")


@csrf_exempt
def add_subscriber(request):
    if (request.method == "POST"):
        response['nama'] = request.POST['nama']
        response['email'] = request.POST['email']
        response['password'] = request.POST['password']
        subscribe = subscribe_models(
            nama=response['nama'], email=response['email'], password=response['password'])
        subscribe.save()
        lst = []
        lst.append(response)
        return JsonResponse(lst, safe=False)
    return HttpResponse("Ini cuma buat nambah aja.")


@csrf_exempt
def validate_email(request):
    email = request.POST.get("email", None)
    check = False
    for i in subscribe_models.objects.filter(email=email):
        if (i.email == email):
            check = True
            break
    data = {
        'is_taken': check
    }
    return JsonResponse(data)
