from django.test import TestCase, Client
from django.urls import resolve
from .views import subscribe
from .models import subscribe_models
from .forms import subscribe_forms

# Create your tests here.
class belajarfilm_subscribe_testcase(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/subscribe/')
        self.assertEqual(response.status_code, 200)

    def test_function_using_function(self):
        found = resolve('/subscribe/')
        self.assertEqual(found.func, subscribe)

    def test_model_can_create_new_subscribe(self):
        subscribe_models.objects.create(nama='supri', email='test@gmail.com', password='af6w7faw07f097f')
        counting_all_available_subscribe = subscribe_models.objects.all().count()
        self.assertEqual(counting_all_available_subscribe, 1)

    def test_form_validation_for_blank_items(self):
        form = subscribe_forms(data={'nama':'', 'email':'', 'password':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['nama'],
            ["This field is required."],
        )
        self.assertEqual(
            form.errors['email'],
            ["This field is required."],
        )
        self.assertEqual(
            form.errors['password'],
            ["This field is required."],
        )


    def test_belajarfilm_post_success_and_render_the_result(self):
        test = 'ests'
        response_post = Client().post('/subscribe/', {'nama':test, 'email':'aiufg3792@ebfsi', 'password':'124egieg'})
        self.assertEqual(response_post.status_code, 200)

    def test_belajarfilm_post_error_invalid_name_and_render_the_result(self):
        test = 'apa saja' * 300
        Client().post("/add/", {'nama':test, 'email':'test@gmail.com', 'password':'fawf8a62'})
        self.assertEqual(subscribe_models.objects.filter(nama=test).count(), 0)
    
    def test_belajarfilm_post_error_invalid_email_and_render_the_result(self):
        Client().post("/add/", {'nama':"test", 'email':'test', 'password':'fawfw9f98aw6f8a6'})
        self.assertEqual(subscribe_models.objects.filter(nama="test").count(), 0)

    def test_belajarfilm_post_error_email_already_taken_and_render_the_result(self):
        Client().post("/add/", {'nama':"test", 'email':'aifgfi73g9@gmail.com', 'password':'fawfw96a6'})
        Client().post("/add/", {'nama':"dwad", 'email':'aifgfi73g9@gmail.com', 'password':'faww6f8a6'})
        self.assertEqual(subscribe_models.objects.filter(nama="test").count(), 0)