from django.urls import path
from .views import *

app_name = 'subscribe'
# url for app
urlpatterns = [
    path('', subscribe, name='subscribe'),
    path('validate', validate_email, name='validate_email'),
    path('add_subscriber', add_subscriber, name='add_subscriber'),
    path('list_subscriber', subscribe_list, name='subscriber_list'),
    path('delete', delete_subscriber, name='delete_subscriber'),
]
