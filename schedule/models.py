from django.db import models


class schedule_model(models.Model):
    categories = (
        ('Penting', 'Penting'),
        ('Santai', 'Santai')
    )
    nama_kegiatan = models.CharField(max_length=50)
    jadwal = models.DateField(auto_now=False, blank=False, null=True)
    jam = models.TimeField()
    tempat = models.CharField(max_length=50)
    kategori = models.CharField(max_length=50, choices=categories)