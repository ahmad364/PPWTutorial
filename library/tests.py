from django.test import TestCase, Client
from django.urls import resolve
from .views import my_library

# Create your tests here.

class belajarfilm_library_testcase(TestCase):
    def test_belajarfilm_url_json_is_exist(self):
        response = Client().get('/library/json')
        self.assertEqual(response.status_code, 200)

    def test_belajarfilm_url_library_is_exist(self):
        response = Client().get('/library/')
        self.assertEqual(response.status_code, 200)

    def test_belajarfilm_using_my_library_function(self):
        found = resolve('/library/')
        self.assertEqual(found.func, my_library)